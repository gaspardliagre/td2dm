package userinfo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import network.UserInfo
import okhttp3.MultipartBody
import tasklist.Task

class UserInfoViewModel : ViewModel() {
    private val userInfoRepository = UserInfoRepository()
    private val _userInfo = MutableLiveData<UserInfo>()
    public val userInfo: LiveData<UserInfo> = _userInfo


    fun getInfo() {
        viewModelScope.launch {
            _userInfo.value = userInfoRepository.getInfo()
        }
    }

    fun updateAvatar(avatar : MultipartBody.Part){
        viewModelScope.launch {
            userInfoRepository.updateAvatar(avatar)
        }
    }

    fun update(user : UserInfo){
        viewModelScope.launch {
            userInfoRepository.update(user)
        }
    }
}