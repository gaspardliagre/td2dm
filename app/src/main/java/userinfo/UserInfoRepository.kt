package userinfo

import network.Api
import network.UserInfo
import okhttp3.MultipartBody
import retrofit2.Response

class UserInfoRepository {
    private val userService = Api.INSTANCE.userService

    suspend fun getInfo() : UserInfo? {
        val response = userService.getInfo().body()!!
        return response

    }

    suspend fun updateAvatar(avatar: MultipartBody.Part){
        userService.updateAvatar(avatar)
    }

    suspend fun update(user: UserInfo){
        userService.update(user)
    }
}