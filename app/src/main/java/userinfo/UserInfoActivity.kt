package userinfo

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.MediaStore.Images.Media.getContentUri
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.core.net.toUri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import coil.load
import com.example.android.td2dm.R
import kotlinx.coroutines.launch
import kotlinx.coroutines.newCoroutineContext
import network.Api
import network.UserInfo
import network.UserService
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

class UserInfoActivity : AppCompatActivity() {

    private val viewModel : UserInfoViewModel by viewModels()
    private val userService = Api.INSTANCE.userService
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)

        val takePictureButton : Button = findViewById(R.id.take_picture_button)
        takePictureButton.setOnClickListener {
            askCameraPermissionAndOpenCamera()
        }

        //viewModel.getInfo()
        lifecycleScope.launch {
            val userInfo = userService.getInfo().body()!!
            val imageView: ImageView = findViewById(R.id.image_view)
            imageView.load(userInfo.avatar)
        }

        val uploadPictureButton : Button = findViewById(R.id.upload_image_button)
        uploadPictureButton.setOnClickListener{
            pickInGallery.launch("image/*")
        }

        val nomText : EditText = findViewById(R.id.nom)
        val prenomText : EditText = findViewById(R.id.prenom)
        val mailText : EditText = findViewById(R.id.mail)

        lifecycleScope.launch {
            val userInfo = userService.getInfo().body()!!
            nomText.setText(userInfo.lastName)
            prenomText.setText(userInfo.firstName)
            mailText.setText(userInfo.email)
        }

        val validerButton : Button = findViewById(R.id.valid_button)
        validerButton.setOnClickListener {
            lifecycleScope.launch {
                val userInfo = userService.getInfo().body()!!
                val firstName = prenomText.text.toString()
                val lastName = nomText.text.toString()
                val avatar = userInfo.avatar
                val mail = mailText.text.toString()
                val newUserInfo : UserInfo = UserInfo(mail, firstName, lastName, avatar)
                userService.update(newUserInfo)
            }
            finish()
        }

    }

    private val requestPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) openCamera()
            else showExplanationDialog()
        }

    private fun requestCameraPermission() =
        requestPermissionLauncher.launch(Manifest.permission.CAMERA)

    private fun askCameraPermissionAndOpenCamera() {
        when {
            ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_GRANTED -> openCamera()
            shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) -> showExplanationDialog()
            else -> requestCameraPermission()
        }
    }

    private fun showExplanationDialog() {
        AlertDialog.Builder(this).apply {
            setMessage("On a besoin de la caméra sivouplé ! 🥺")
            setPositiveButton("Bon, ok") { _, _ ->
                requestCameraPermission()
            }
            setCancelable(true)
            show()
        }
    }

    // create a temp file and get a uri for it
    private val photoUri = getContentUri("temp")

    // register
    private val takePicture = registerForActivityResult(ActivityResultContracts.TakePicturePreview()) { bitmap ->
        val tmpFile = File.createTempFile("avatar", "jpeg")
        tmpFile.outputStream().use {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
        }
        handleImage(tmpFile.toUri())
    }


    // use
    private fun openCamera() = takePicture.launch()

    // convert
    private fun convert(uri: Uri) =
        MultipartBody.Part.createFormData(
            name = "avatar",
            filename = "temp.jpeg",
            body = contentResolver.openInputStream(uri)!!.readBytes().toRequestBody())

    private fun handleImage(uri: Uri) {
        val newImage = convert(uri)
        val imageView : ImageView = findViewById(R.id.image_view)
        //viewModel.updateAvatar(newImage)
        //viewModel.getInfo()
        //imageView.load(viewModel.userInfo.value?.avatar)
        lifecycleScope.launch {
            userService.updateAvatar(newImage)
            val userInfo = userService.getInfo().body()!!
            imageView.load(userInfo.avatar)
        }
    }

    // register
    private val pickInGallery =
        registerForActivityResult(ActivityResultContracts.GetContent()) { uri ->
            handleImage(uri)
        }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        val name = sharedPreferences.getString("Titre", "")
        val actionBar = supportActionBar
        actionBar?.title = name;

        val color_bar = sharedPreferences.getString("Couleur_AppBar", "#FF6200EE")
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor(color_bar)))
    }
}