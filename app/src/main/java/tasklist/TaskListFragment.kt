package tasklist

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.constraintlayout.solver.state.State
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.example.android.td2dm.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.launch
import network.Api
import network.AuthenticationActivity
import network.SHARED_PREF_TOKEN_KEY
import network.TasksRepository
import task.TaskActivity
import task.TaskActivity.Companion.ADD_TASK_REQUEST_CODE
import userinfo.UserInfoActivity
import java.util.*

class TaskListFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel.loadTasks();
        val rootView = inflater.inflate(R.layout.fragment_task_list, container, false);
        return rootView;
    }

    private lateinit var myAdapter: TaskListAdapter
    private val viewModel: TaskListViewModel by viewModels()

    val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            val data = it.data // Et non pas "result.intent"
            // Handle the Intent
            val task = data!!.getSerializableExtra("newTask") as? Task
            if (task != null) {
                viewModel.addTask(task)
            };
            val task2 = data!!.getSerializableExtra("modifyTask") as? Task
            if (task2 != null) {
                viewModel.editTask(task2)
            };
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(activity);
        recyclerView.adapter = TaskListAdapter();
        this.myAdapter = (recyclerView.adapter as TaskListAdapter);

        viewModel.taskList.observe(viewLifecycleOwner, androidx.lifecycle.Observer { newList ->
            myAdapter.taskList = newList.orEmpty()
        })

        val floatButton : FloatingActionButton = view.findViewById(R.id.floatingActionButton);
        floatButton.setOnClickListener {
            val intent = Intent(activity, TaskActivity::class.java)
            startForResult.launch(intent)
        }

        val decoButton : Button = view.findViewById(R.id.deconnexion)
        decoButton.setOnClickListener {
            PreferenceManager.getDefaultSharedPreferences(context).edit {
                putString(SHARED_PREF_TOKEN_KEY, "")
            }
            startActivity(Intent(activity, AuthenticationActivity::class.java))
        }

        myAdapter.onDeleteClickListener = { task ->
            // Supprimer la tâche
            viewModel.deleteTask(task)

        }

        myAdapter.onEditClickListener = {task ->
            val intent = Intent(activity, TaskActivity::class.java)
            intent.putExtra("task", task);
            startForResult.launch(intent)
        }

        val imageView : ImageView = view.findViewById(R.id.avatar)
        imageView.setOnClickListener {
            val intent = Intent(activity, UserInfoActivity::class.java)
            startActivity(intent)
        }
    }

   override fun onResume() {
        super.onResume()
        lifecycleScope.launch {
            val userInfo = Api.INSTANCE.userService.getInfo().body()!!
            val my_text_view = requireView().findViewById<TextView>(R.id.info)
            my_text_view.setText("${userInfo?.firstName} ${userInfo?.lastName}")

            val image_view = requireView().findViewById<ImageView>(R.id.avatar)
            image_view.load(userInfo?.avatar){
                transformations(CircleCropTransformation());
            }

       }

       val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
       val color_police = sharedPreferences.getString("Couleur_police", "#FF000000")
       val task_title : TextView? = view?.findViewById(R.id.task_title)
       val task_description : TextView? = view?.findViewById(R.id.task_description)
       if (color_police != null) {
           task_title?.setTextColor(Color.parseColor(color_police))
           task_description?.setTextColor(Color.parseColor(color_police))
       }

    }

}

