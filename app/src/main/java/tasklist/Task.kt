package tasklist

import android.icu.text.CaseMap
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName

@Serializable
data class Task (@SerialName("id") val id : String,
                 @SerialName("title") var title: String,
                 @SerialName("description") var description: String = " ") : java.io.Serializable{

                 }


