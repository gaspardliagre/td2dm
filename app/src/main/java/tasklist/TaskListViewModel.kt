package tasklist

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import network.TasksRepository

class TaskListViewModel : ViewModel() {
    private val tasksRepository = TasksRepository()
    private val _taskList = MutableLiveData<List<Task>>()
    val taskList: LiveData<List<Task>> = _taskList

    fun loadTasks() {
        viewModelScope.launch {
            _taskList.value = tasksRepository.loadTasks()
        }
    }

    fun deleteTask(task: Task) {
        viewModelScope.launch {
            tasksRepository.deleteTask(task)
        }
        val list = _taskList.value.orEmpty().toMutableList()
        list.remove(task)
        _taskList.value = list
    }

    fun addTask(task: Task) {
        viewModelScope.launch {
            tasksRepository.createTask(task)
        }
        val list = _taskList.value.orEmpty().toMutableList()
        list.add(task)
        _taskList.value = list
    }

    fun editTask(task: Task) {
        viewModelScope.launch {
            val editedTask = tasksRepository.updateTask(task)
            val editableList = _taskList.value.orEmpty().toMutableList()
            val position = editableList.indexOfFirst { task.id == it.id }
            if (editedTask != null) {
                editableList[position] = editedTask
            }
            _taskList.value = editableList
        }
    }
}