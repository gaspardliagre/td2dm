package tasklist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.android.td2dm.R
import kotlin.properties.Delegates

class TaskListAdapter : ListAdapter<Task, TaskListAdapter.TaskViewHolder>(TaskDiffCallback()) {

    var taskList: List<Task> by Delegates.observable(emptyList()) { _, _, _ ->
        notifyDataSetChanged()
    }

    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(task : Task) {
            itemView.apply { // `apply {}` permet d'éviter de répéter `itemView.*`
                // TODO: afficher les données et attacher les listeners aux différentes vues de notre [itemView]
                val title : TextView = findViewById<TextView>(R.id.task_title);
                title.setText(task.title);
                val description : TextView = findViewById<TextView>(R.id.task_description);
                description.setText(task.description);
                findViewById<ImageButton>(R.id.delete_button).setOnClickListener {
                    onDeleteClickListener?.invoke(task)
                }
                findViewById<ImageButton>(R.id.edit_button).setOnClickListener {
                    onEditClickListener?.invoke(task)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false);
        return TaskViewHolder(itemView);
    }

    override fun getItemCount(): Int {
        return taskList.size;
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(taskList[position]);
    }

    var onDeleteClickListener: ((Task) -> Unit)? = null

    var onEditClickListener: ((Task) -> Unit)? = null

}