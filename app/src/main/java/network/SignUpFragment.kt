package network

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.example.android.td2dm.MainActivity
import com.example.android.td2dm.R
import kotlinx.coroutines.launch

class SignUpFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_signup, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val signUpButton : Button = view.findViewById(R.id.créer_compte)
        val firstNameText : EditText = view.findViewById(R.id.first_name)
        val lastNameText : EditText = view.findViewById(R.id.last_name)
        val mailText : EditText = view.findViewById(R.id.mail_signup)
        val passwordText : EditText = view.findViewById(R.id.password_signup)
        val passwordConfirmText : EditText = view.findViewById(R.id.password_confirm)

        signUpButton.setOnClickListener {
            if(firstNameText.text.isNotBlank() && lastNameText.text.isNotBlank() && mailText.text.isNotBlank() && passwordText.text.isNotBlank() && passwordConfirmText.text.isNotBlank()){
                val signUpForm= SignUpForm(firstNameText.text.toString(), lastNameText.text.toString(), mailText.text.toString(), passwordText.text.toString(), passwordConfirmText.text.toString())
                lifecycleScope.launch {
                    val result = Api.INSTANCE.userService.signUp(signUpForm)
                    val realToken = result.body()
                    if (result.isSuccessful){
                        PreferenceManager.getDefaultSharedPreferences(context).edit {
                            putString(SHARED_PREF_TOKEN_KEY, realToken?.token)
                        }
                        startActivity(Intent(activity, MainActivity::class.java))
                    }
                    else{
                        Toast.makeText(context, "Problème de connexion", Toast.LENGTH_LONG).show()
                    }
                }

            }
            else{
                Toast.makeText(context, "Veuillez remplir tous les champs", Toast.LENGTH_LONG).show()
            }
        }
    }
}
