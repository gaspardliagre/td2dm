package network

import android.content.Intent
import android.os.Bundle
import android.provider.Settings.Global.putString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.core.content.edit
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.preference.PreferenceManager
import com.example.android.td2dm.MainActivity
import com.example.android.td2dm.R
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_login, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val logButton : Button = view.findViewById(R.id.connecter)
        val mailText : EditText = view.findViewById(R.id.email)
        val passwordText : EditText = view.findViewById(R.id.password)

        logButton.setOnClickListener {
            if(mailText.text.isNotBlank() && passwordText.text.isNotBlank()){
                val loginForm = LoginForm(mailText.text.toString(), passwordText.text.toString())
                lifecycleScope.launch {
                   val userService = Api.INSTANCE.userService
                    val realToken = userService.login(loginForm).body()
                    if (realToken != null){
                        PreferenceManager.getDefaultSharedPreferences(context).edit {
                            putString(SHARED_PREF_TOKEN_KEY, realToken.token)
                        }
                        startActivity(Intent(activity, MainActivity::class.java))
                    }
                    else{
                        Toast.makeText(context, "Problème de connexion", Toast.LENGTH_LONG).show()
                    }
                }
            }
            else{
                Toast.makeText(context, "Veuillez remplir tous les champs", Toast.LENGTH_LONG).show()
            }
        }
    }
}