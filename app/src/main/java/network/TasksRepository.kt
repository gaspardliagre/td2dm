package network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import tasklist.Task
import tasklist.TaskListViewModel

class TasksRepository {
    private val tasksWebService = Api.INSTANCE.tasksWebService

    suspend fun loadTasks(): List<Task>? {
        val response = tasksWebService.getTasks()
        return if (response.isSuccessful) response.body() else null
    }

    suspend fun updateTask(task : Task): Task? {
        val editedTask = tasksWebService.updateTask(task).body()
        return editedTask
    }

    suspend fun createTask(task : Task) {
        tasksWebService.createTask(task)

    }

    suspend fun deleteTask(task: Task) {
        tasksWebService.deleteTask(task.id)
    }
}