package network

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.navigation.fragment.findNavController
import com.example.android.td2dm.MainActivity
import com.example.android.td2dm.R

class AuthenticationFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_authentication, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val logButton : Button = view.findViewById(R.id.login)
        val signButton : Button = view.findViewById(R.id.signup)

        val token = Api.INSTANCE.getToken()

        logButton.setOnClickListener {
            findNavController().navigate(R.id.action_authenticationFragment_to_loginFragment)
        }

        signButton.setOnClickListener {
            findNavController().navigate(R.id.action_authenticationFragment_to_signUpFragment)
        }

        if(token != ""){
            startActivity(Intent(activity, MainActivity::class.java))
        }
    }
}