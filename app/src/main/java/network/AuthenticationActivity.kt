package network

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.preference.PreferenceManager
import com.example.android.td2dm.R

class AuthenticationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_authentication)
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        val name = sharedPreferences.getString("Titre", "")
        val actionBar = supportActionBar
        actionBar?.title = name;

        val color_bar = sharedPreferences.getString("Couleur_AppBar", "#FF6200EE")
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor(color_bar)))
    }
}