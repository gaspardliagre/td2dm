package com.example.android.td2dm

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.TextView
import androidx.preference.PreferenceManager
import settings.MySettingsActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.settings -> {
                startActivity(Intent(this, MySettingsActivity::class.java))
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        val name = sharedPreferences.getString("Titre", "")
        val actionBar = supportActionBar
        actionBar?.title = name;

        val color_bar = sharedPreferences.getString("Couleur_AppBar", "#FF6200EE")
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor(color_bar)))

        val color_police = sharedPreferences.getString("Couleur_police", "#FF000000")
        val info : TextView = findViewById(R.id.info)
        if (color_police != null) {
            info.setTextColor(Color.parseColor(color_police))

        }

    }


}