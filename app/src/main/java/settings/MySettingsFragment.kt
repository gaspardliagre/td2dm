package settings

import android.app.ActionBar
import android.os.Bundle
import androidx.preference.EditTextPreference
import androidx.preference.PreferenceFragmentCompat
import com.example.android.td2dm.R

class MySettingsFragment : PreferenceFragmentCompat (){
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, rootKey)
        val editText : EditTextPreference? = findPreference("Titre");
        val editTitle : String = editText.toString().trim();


    }
}