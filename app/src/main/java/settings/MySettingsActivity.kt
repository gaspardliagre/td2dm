package settings

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.Window
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.WindowDecorActionBar
import androidx.core.graphics.toColor
import androidx.core.graphics.toColorInt
import androidx.datastore.core.DataStore
import androidx.datastore.createDataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.preference.PreferenceManager
import com.example.android.td2dm.R

class MySettingsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_settings)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings_container, MySettingsFragment())
                .commit()

        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        val name = sharedPreferences.getString("Titre", "")
        val actionBar = supportActionBar
        actionBar?.title = name;

        val color_bar = sharedPreferences.getString("Couleur_AppBar", "#FF6200EE")
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor(color_bar)))

    }

    val dataStore: DataStore<Preferences> = this.createDataStore(
        name = "settings"
    )
}