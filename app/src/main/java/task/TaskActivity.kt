package task

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.preference.PreferenceManager
import com.example.android.td2dm.R
import tasklist.Task
import tasklist.TaskListFragment
import java.util.*

class TaskActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_task)
        var task = null as? Task;

        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent) // Handle text being sent
                }
            }
            else -> {
                task = intent!!.getSerializableExtra("task") as? Task
                findViewById<EditText>(R.id.titre).setText(task?.title);
                findViewById<EditText>(R.id.description).setText(task?.description);
            }
        }

        var ValidButton : Button = findViewById(R.id.valider);
        ValidButton.setOnClickListener {
            val id : String = task?.id ?: UUID.randomUUID().toString();
            val titre : String = findViewById<EditText>(R.id.titre).editableText.toString();
            val description : String = findViewById<EditText>(R.id.description).editableText.toString();
            val newTask : Task = Task(id = id, title = titre, description = description);
            if(task == null){
                intent.putExtra("newTask", newTask);
            }
            else{
                intent.putExtra("modifyTask", newTask);
            }
            setResult(Activity.RESULT_OK, intent);
            finish();

        }
    }

    private fun handleSendText(intent: Intent) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            // Update UI to reflect text being shared
            findViewById<EditText>(R.id.description).setText(it);
        }
    }

    companion object {
        const val ADD_TASK_REQUEST_CODE = 666
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        val name = sharedPreferences.getString("Titre", "")
        val actionBar = supportActionBar
        actionBar?.title = name;

        val color_bar = sharedPreferences.getString("Couleur_AppBar", "#FF6200EE")
        actionBar?.setBackgroundDrawable(ColorDrawable(Color.parseColor(color_bar)))
    }
}